"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ko = require("knockout");
var $ = require("jquery");
var KendoColorPicker = (function () {
    function KendoColorPicker() {
        this.init = function (el, va, ab, vm, bc) {
            var vac = ko.unwrap(va());
            $(el).kendoColorPicker();
            var colorPicker = $(el).data("kendoColorPicker");
            colorPicker.value(vac);
            colorPicker.bind("select", selected_color);
            function selected_color(e) {
                va()(e.value);
            }
        };
    }
    return KendoColorPicker;
}());
exports.KendoColorPicker = KendoColorPicker;
ko.bindingHandlers.kendoColorPicker = new KendoColorPicker();
