"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ko = require("knockout");
var $ = require("jquery");
function defaultButton(obs) {
    return [
        {
            text: 'Close',
            action: function () {
                obs(false);
            }
        }
    ];
}
var KendoDialog = (function () {
    function KendoDialog() {
        this.init = function (el, va, ab, vm, bc) {
            var vac = va();
            var w = ko.unwrap(ab.get('kd-width')) || 100;
            var h = ko.unwrap(ab.get('kd-height')) || 100;
            var m = ko.unwrap(ab.get('kd-modal'));
            var c = ko.unwrap(ab.get('kd-closable')) || true;
            var t = ko.unwrap(ab.get('kd-title')) || "Dialog";
            var a = ko.unwrap(ab.get('kd-actions')) || defaultButton(vac);
            var dialog = $(el).kendoDialog({
                width: w,
                height: h,
                title: t,
                closable: c,
                modal: m,
                actions: a,
                close: function () {
                    vac(false);
                }
            });
        };
        this.update = function (el, va, ab, vm, bc) {
            var $el = $(el);
            var vac = va();
            ko.computed(function () {
                if (vac()) {
                    $(el).data("kendoDialog").open();
                }
                else {
                    $(el).data("kendoDialog").close();
                }
            });
        };
    }
    return KendoDialog;
}());
exports.KendoDialog = KendoDialog;
ko.bindingHandlers.kendoDialog = new KendoDialog();
