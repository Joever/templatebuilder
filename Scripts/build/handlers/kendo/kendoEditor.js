var ko = require("knockout");
var $ = require("jquery"); 

var insertButtonCustomComp = (vm) => {
  return {
    name: "insert-button",
    tooltip: "Insert Button",
    exec: function (e) {
      var props = vm.button;
      props.IsDialogOpen(true);
      var editor = $(this).data("kendoEditor");
      var range = editor.getRange();

      ko.computed(() => {
        if (props.InsertButtonNow()) {
          editor.paste(props.ButtonHtml(), { split: false });
          props.IsDialogOpen(false);
          props.InsertButtonNow(false);
        }
      });
    }
  }
};

var imageInsertCustomComp = (vm) => {

  return {
    name: "custom",
    tooltip: "Insert Image",
    exec: function (e) {
      var props = vm.image;
      props.IsDialogOpen(true);
      var editor = $(this).data("kendoEditor");
      var range = editor.getRange();

      ko.computed(() => {
        if (props.InsertImageNow()) {
          var imageHtml = `<img src="${props.src()}" width="${props.width()}" height="${props.height()}" , alt="${props.alt()}" /> `;
          editor.exec('inserthtml', { value: imageHtml });
          props.IsDialogOpen(false);
          props.InsertImageNow(false);
        }
      });
    }
  };
};


ko.bindingHandlers.kendoEditor = {
  init: (el, va, ab, vm, bc) => {
    var editor = $(el).kendoEditor({
      tools: [
        "bold",
        "italic",
        "underline",
        "createLink",
        "unlink",
        insertButtonCustomComp(vm),
        imageInsertCustomComp(vm),
         "insertImage","viewHtml"
      ],
              encoded: false,
      imageBrowser: {
        messages: {
          dropFilesHere: "Drop files here"
        },
        transport: {
          read: "/kendo-ui/service/ImageBrowser/Read",
          destroy: {
            url: "/kendo-ui/service/ImageBrowser/Destroy",
            type: "POST"
          },
          create: {
            url: "/kendo-ui/service/ImageBrowser/Create",
            type: "POST"
          },
          thumbnailUrl: "/kendo-ui/service/ImageBrowser/Thumbnail",
          uploadUrl: "/kendo-ui/service/ImageBrowser/Upload",
          imageUrl: "/kendo-ui/service/ImageBrowser/Image?path={0}"
        }
      }

    }).data("kendoEditor");

    function contentChange() {
      var observable = va();
      observable(editor.value());
    }
    editor.bind("change", contentChange);
    editor.bind("keyup", contentChange);
    editor.bind("keydown", contentChange);
  },
  update: function (el, va, ab, vm, bc) {
    var content = va();
    var editor = $(el).data("kendoEditor");

    if (content != undefined) {
      editor.value(content());
    } 
  }
};