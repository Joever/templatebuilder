"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ko = require("knockout");
require("./components");
require("./bindinghandlers");
var $ = require("jquery");
require("knockout.validation");
function init() {
    var viewModel = function () {
    };
    ko.applyBindings(new viewModel());
}
$(document).ready(init);
