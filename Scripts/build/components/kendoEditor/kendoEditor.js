"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var kEditorViewModel = (function () {
    function kEditorViewModel(params) {
        var _this = this;
        this.actionButtons = function () {
            return [
                {
                    text: "Done",
                    action: function () {
                        _this.isDialogOpen(false);
                    }
                }
            ];
        };
        this.isDialogOpen = params.isDialogOpen;
        this.content = params.content;
    }
    return kEditorViewModel;
}());
exports.kEditorViewModel = kEditorViewModel;
