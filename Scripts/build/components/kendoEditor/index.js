"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ko = require("knockout");
var kendoEditor_1 = require("./kendoEditor");
ko.components.register('kEditor-Component', {
    viewModel: kendoEditor_1.kEditorViewModel,
    template: require("./html/kendoEditor.html")
});
