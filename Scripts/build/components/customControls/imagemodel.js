"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ko = require("knockout");
var ImageDefaultStyle = (function () {
    function ImageDefaultStyle() {
        this.margin = ko.observable(0);
        this.padding = ko.observable(0);
        this.border = ko.observable('none');
        this.display = ko.observable('');
        this.alt = ko.observable('');
    }
    return ImageDefaultStyle;
}());
exports.ImageDefaultStyle = ImageDefaultStyle;
var ImageModel = (function () {
    function ImageModel() {
        this.src = ko.observable();
        this.width = ko.observable();
        this.height = ko.observable();
        this.border = ko.observable();
        this.styles = ko.observable();
    }
    return ImageModel;
}());
exports.ImageModel = ImageModel;
;
