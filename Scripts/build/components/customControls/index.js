"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ko = require("knockout");
var button_1 = require("./button");
var image_1 = require("./image");
ko.components.register('Button-Custom-Component', {
    viewModel: button_1.ButtonViewModel,
    template: require("./html/button.html")
});
ko.components.register('Image-Custom-Component', {
    viewModel: image_1.ImageViewModel,
    template: require('./html/imagecontainer.html')
});
