"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ImageViewModel = (function () {
    function ImageViewModel(params) {
        var _this = this;
        this.actionButtons = function () {
            return [
                {
                    text: "Insert",
                    action: function () {
                        _this.insertImageNow(true);
                        _this.isDialogOpen(false);
                    }
                }, {
                    text: "Close",
                    action: function () {
                        _this.isDialogOpen(false);
                    }
                }
            ];
        };
        this.isDialogOpen = params.IsDialogOpen;
        this.insertImageNow = params.InsertImageNow;
        this.imageModel = params;
    }
    return ImageViewModel;
}());
exports.ImageViewModel = ImageViewModel;
