"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ko = require("knockout");
var ImageViewModel = (function () {
    function ImageViewModel() {
        this.image = ko.observable();
        this.IsDialogOpen = ko.observable(false);
    }
    return ImageViewModel;
}());
exports.ImageViewModel = ImageViewModel;
