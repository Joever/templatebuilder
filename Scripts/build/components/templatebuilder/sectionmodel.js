"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ko = require("knockout");
var defaultmodel_1 = require("./defaultmodel");
var SectionViewModel = (function () {
    function SectionViewModel(params) {
        var _this = this;
        this.content = ko.observable('Content here');
        this.model = params;
        this.IsRownDeleted = params.IsRownDeleted;
        this.columns = params.Columns;
        this.addCol = function () {
            _this.columns.push(new defaultmodel_1.Column());
        };
        this.colClass = ko.computed(function () {
            var colNum = 100 / _this.columns().length - 1;
            return "width:" + colNum + '%';
        });
        this.colWidth = ko.computed(function () {
            var col = 600 / _this.columns().length;
            return col;
        });
        this.deleteSec = function () {
            _this.IsRownDeleted(false);
        };
        ko.computed(function () {
            var col = _this.columns()
                .filter(function (c) { return c.IsColumnDeleted() === false; })[0];
            if (col) {
                _this.columns.remove(col);
            }
        });
    }
    return SectionViewModel;
}());
exports.SectionViewModel = SectionViewModel;
