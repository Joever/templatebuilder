"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ko = require("knockout");
var ContentDefinition = (function () {
    function ContentDefinition() {
        this.Row = ko.observableArray();
        this.BodyStyle = ko.observableArray([
            new BodyStyle('margin', '5%')
        ]);
        this.visible = ko.observable(true);
    }
    return ContentDefinition;
}());
exports.ContentDefinition = ContentDefinition;
;
var Row = (function () {
    function Row() {
        this.Columns = ko.observableArray([new Column]);
        this.Css = new DefaultStyle();
        this.Attributes = [
            new AttributesKeyValue('width', '100%')
        ];
        this.IsRownDeleted = ko.observable(true);
    }
    return Row;
}());
exports.Row = Row;
;
var DefaultStyle = (function () {
    function DefaultStyle() {
    }
    return DefaultStyle;
}());
exports.DefaultStyle = DefaultStyle;
var AttributesKeyValue = (function () {
    function AttributesKeyValue(key, value) {
        this.key = key;
        this.value = value;
    }
    return AttributesKeyValue;
}());
exports.AttributesKeyValue = AttributesKeyValue;
;
var Column = (function () {
    function Column() {
        this.Content = ko.observable("Content");
        this.width = ko.observable("");
        this.height = ko.observable("");
        this.Css = new DefaultStyle();
        this.Attributes = [
            new AttributesKeyValue('align', 'center')
        ];
        this.IsColumnDeleted = ko.observable(true);
    }
    return Column;
}());
exports.Column = Column;
;
var BodyStyle = (function () {
    function BodyStyle(key, value) {
        this.key = key;
        this.value = value;
    }
    return BodyStyle;
}());
exports.BodyStyle = BodyStyle;
;
