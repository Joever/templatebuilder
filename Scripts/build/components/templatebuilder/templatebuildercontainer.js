"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ko = require("knockout");
var defaultmodel_1 = require("./defaultmodel");
var TemplateBuilderViewModel = (function () {
    function TemplateBuilderViewModel() {
        var _this = this;
        this.contentDefinition = new defaultmodel_1.ContentDefinition();
        this.sections = this.contentDefinition.Row;
        this.newSection = function () {
            _this.sections.push(new defaultmodel_1.Row());
        };
        this.SaveTemplate = function () {
            console.log(ko.toJS(_this.contentDefinition));
        };
        ko.computed(function () {
            var secsRemoved = _this.sections()
                .filter(function (c) { return c.IsRownDeleted() === false; })[0];
            if (secsRemoved) {
                _this.sections.remove(secsRemoved);
            }
            var secColCount = _this.sections().filter(function (c) { return c.Columns().length === 0; })[0];
            if (secColCount) {
                _this.sections.remove(secColCount);
            }
        });
    }
    return TemplateBuilderViewModel;
}());
exports.TemplateBuilderViewModel = TemplateBuilderViewModel;
