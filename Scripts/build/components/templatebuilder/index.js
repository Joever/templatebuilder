"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ko = require("knockout");
var TemplateBuilderContainer_1 = require("./TemplateBuilderContainer");
var sectionmodel_1 = require("./sectionmodel");
var columnmodel_1 = require("./columnmodel");
ko.components.register('TemplateBuilderContainer-Component', {
    viewModel: TemplateBuilderContainer_1.TemplateBuilderViewModel,
    template: require("./html/templatebuildercontainer.html")
});
ko.components.register('SectionModel-Component', {
    viewModel: sectionmodel_1.SectionViewModel,
    template: require("./html/sectionmodel.html")
});
ko.components.register('ColumnModel-Component', {
    viewModel: columnmodel_1.ColumnsModel,
    template: require("./html/columnmodel.html")
});
