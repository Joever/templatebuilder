"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ko = require("knockout");
var chai_1 = require("chai");
var templatebuildercontainer_1 = require("./../components/templatebuilder/templatebuildercontainer");
var defaultmodel_1 = require("./../components/templatebuilder/defaultmodel");
var sectionmodel_1 = require("./../components/templatebuilder/sectionmodel");
var columnmodel_1 = require("./../components/templatebuilder/columnmodel");
describe('TemplateBuilder', function () {
    it('should add new row/section when newSection method is trigger.', function () {
        var templateBuilderModel = new templatebuildercontainer_1.TemplateBuilderViewModel();
        templateBuilderModel.newSection();
        chai_1.expect(templateBuilderModel.sections().length).to.eq(1);
    });
    it('should add new colum in row/section when addCol method is trigger.', function () {
        var visible = ko.observable(true);
        var section = new sectionmodel_1.SectionViewModel(new defaultmodel_1.Row()); //create default one column when initialized new row.
        var column = new columnmodel_1.ColumnsModel({ visible: visible });
        section.addCol(column);
        section.addCol(column);
        section.addCol(column);
        chai_1.expect(section.columns().length).to.equal(4);
    });
    it('should return width: 19% if 100% and column length-1 is 5 (100/5).', function () {
        var percent = 100;
        var colLength = 5;
        var length = percent / colLength - 1;
        chai_1.expect('width:' + length + '%').to.equal('width:' + 19 + '%');
    });
    it('should delete row/section when deleteSec is trigger.', function () {
        var section = new sectionmodel_1.SectionViewModel(new defaultmodel_1.Row()); //create default one column when initialized new row.
        section.deleteSec();
        chai_1.expect(section.IsRownDeleted()).to.equal(false);
    });
    it('should delte column when deleteCol method is trigger', function () {
        var visible = ko.observable(true);
        var column = new columnmodel_1.ColumnsModel({ visible: visible });
        column.deleteCol();
        chai_1.expect(column.IsColumnDeleted()).to.equal(false);
    });
});
