import * as ko from "knockout";
import * as $ from "jquery";
import { kEditorViewModel } from "./kendoEditor" 
 
ko.components.register('kEditor-Component', {
    viewModel: kEditorViewModel,
    template: require("./html/kendoEditor.html")
});

 