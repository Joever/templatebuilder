import * as ko from "knockout";
import * as $ from "jquery";
import {ButtonViewModel} from "./button"
import { ImageViewModel } from './image';

ko.components.register('Button-Custom-Component', {
    viewModel: ButtonViewModel,
    template: require("./html/button.html")
});


ko.components.register('Image-Custom-Component', {
    viewModel : ImageViewModel,
    template: require('./html/imagecontainer.html')
});