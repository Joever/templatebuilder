import * as ko from "knockout";
import * as $ from "jquery";
import * as model from "../kendoEditor/kendoEditor";

interface buttonParamModel{
    dialogButton: KnockoutObservable<boolean>;
    components: KnockoutObservableArray<any>;        
}

export class ButtonViewModel {
    button: ButtonModel;
    components: KnockoutObservableArray<any>;
    dialogButton: KnockoutObservable<boolean>;

    constructor(params: buttonParamModel) {
        this.button = new ButtonModel();
        this.dialogButton = params.dialogButton;
        this.components = params.components;
    }

    actionButtons = () => {
        return [
            {
                text: "Insert",
                action: () => {
                    this.components.push(this.button);
                    this.button = new ButtonModel();
                    console.log(this.components());
                    this.dialogButton(false);
                }
            }, {
                text: "Close",
                action: () => {
                    this.dialogButton(false);
                }
            }
        ]
    }
}

export class ButtonModel {
    IsDialogOpen = ko.observable<boolean>(false);
    InsertButtonNow = ko.observable<boolean>(false);
    table = new TableModel();
    styles = new StylesModel();
    anchor = new AnchorModel();
    ButtonHtml = ko.computed(() => {
        return `<table 
                    width="${this.table.width()}"
                    align="${this.table.align()}" 
                    height="${this.table.height()}" 
                    cellpadding="0" 
                    cellspacing="0" 
                    border="0" 
                    style="
                        background-color:${this.table.backgroundcolor()}; 
                        border-radius:4px;
                    ">
                  <tr>
                    <td 
                        align="center" 
                        valign="middle" 
                        height="${this.table.height()}" 
                        style="
                            font-family: ${this.styles.fontFamily()}, sans-serif; 
                            font-size:${this.styles.fontSize()}px; 
                            font-weight:Bold;
                        ">
                      <a 
                        href="${this.anchor.href()}" 
                        target="_blank" 
                        style="
                            font-family: ${this.styles.fontFamily()}, sans-serif; 
                            color:${this.anchor.color()}; 
                            display: inline-block; 
                            text-decoration: none; 
                            width:${this.table.width()}px; 
                            font-weight:Bold;
                        ">
                        ${this.table.text()}
                      </a>
                    </td>
                  </tr>
                </table>`
    }, this);
}

export class TableModel {
    text = ko.observable<string>("Button");
    height = ko.observable<number>(44);
    width = ko.observable<number>(200);
    alignOptions = ko.observableArray<string>(['left', 'right', 'center']);
    align = ko.observable<string>('left');
    backgroundcolor = ko.observable<string>('#2b3a63');
}

export class StylesModel {
    fontFamily = ko.observable<string>("Arial");
    fontSize = ko.observable<string>("14px");
}

export class AnchorModel {
    href = ko.observable<string>("#");
    color = ko.observable<string>("#ffffff");
}

