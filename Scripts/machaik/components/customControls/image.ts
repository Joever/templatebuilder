import * as ko from "knockout";
import * as $ from "jquery";
import * as model from "../kendoEditor/kendoEditor";


interface imageParamModel {
    dialogButton: KnockoutObservable<boolean>;
    components: KnockoutObservableArray<any>;
};

export class ImageViewModel {

    image: ImageModel;
    components: KnockoutObservableArray<any>;
    dialogButton: KnockoutObservable<boolean>;

    constructor(params: imageParamModel) {
        this.image = new ImageModel();
        this.dialogButton = params.dialogButton;
        this.components = params.components;
    }

    actionButtons = () => {

        return [
            {
                text: "Insert",
                action: () => {
                    this.components.push(this.image);
                    this.image = new ImageModel();
                    console.log(this.components());
                    this.dialogButton(false);
                }
            }, {
                text: "Close",
                action: () => {
                    this.dialogButton(false);
                }
            }
        ]

    };
}

export class ImageModel {
    IsDialogOpen = ko.observable<boolean>(false);
    InsertImageNow = ko.observable<boolean>(false);
    src = ko.observable<string>('');
    width = ko.observable<number>(100);
    height = ko.observable<number>(100);
    alt = ko.observable<string>('');
    ImageHtml = ko.computed(() => {

         `<img src="${this.src()}" width="${this.width()}" height="${this.height()}" , alt="${this.alt()}" /> `;
    });

};

