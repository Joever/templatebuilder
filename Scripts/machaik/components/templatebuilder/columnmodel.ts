﻿import * as ko from "knockout";
import { kEditorViewModel } from "../kendoEditor/kendoEditor"
import { ButtonModel } from "../customControls/button";
import { ImageModel } from "../customControls/image";

export class ColumnsModel {
    content = ko.observable<string>('');
    isDialogOpen = ko.observable<boolean>(false);
    IsColumnDeleted: KnockoutObservable<boolean>;
    dialogButton = ko.observable<boolean>(false);
    image = new ImageModel();
    components = ko.observableArray<any>([]);

    constructor(params) {
        this.IsColumnDeleted = params.IsColumnDeleted;        
    }
    deleteCol = () => {
        this.IsColumnDeleted(false);
    }

    insertText = () => {
        //this.components.push(new kEditorViewModel());
    }

    openButton = () => {
        this.dialogButton(true);
    }

    openImage = () => {
        this.dialogButton(true);
    }

    insertVideo = () => {
    }
}
