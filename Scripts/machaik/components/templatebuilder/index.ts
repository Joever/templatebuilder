﻿import * as ko from "knockout";
import { TemplateBuilderViewModel } from "./TemplateBuilderContainer";
import { SectionViewModel } from "./sectionmodel";
import { ColumnsModel } from "./columnmodel";
 
ko.components.register('TemplateBuilderContainer-Component', {
    viewModel: TemplateBuilderViewModel,
    template: require("./html/templatebuildercontainer.html")
})

ko.components.register('SectionModel-Component', {
    viewModel: SectionViewModel,
    template: require("./html/sectionmodel.html")
})

ko.components.register('ColumnModel-Component', {
    viewModel: ColumnsModel,
    template: require("./html/columnmodel.html")
})


