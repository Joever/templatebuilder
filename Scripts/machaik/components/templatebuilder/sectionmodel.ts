﻿import * as ko from "knockout"; 
import {Row, Column} from "./defaultmodel";


export class SectionViewModel
{
    model: Row;
    content = ko.observable<string>('Content here');
    columns: KnockoutObservableArray<Column>;
    IsRownDeleted: KnockoutObservable<boolean>;
    addCol: Function;
    colClass: Function;
    deleteSec: Function;
    colWidth: Function;
    constructor(params: Row) 
    {
        this.model = params;
        this.IsRownDeleted = params.IsRownDeleted;
        this.columns = params.Columns;
        
        this.addCol = () =>
        {
            this.columns.push(new Column());
        }
        
        this.colClass = ko.computed(() =>
        {
            let colNum = 100 / this.columns().length - 1;
            return "width:" + colNum + '%';
        });

          this.colWidth = ko.computed(() => {
            let col = 600 / this.columns().length;
            return col;
        }) 


        this.deleteSec = () =>
        {
            this.IsRownDeleted(false);
        }

        ko.computed(() =>
        {
            let col = this.columns()
                .filter(c => c.IsColumnDeleted() === false)[0];

            if (col) {
                this.columns.remove(col);
            }           
        });
    }    
}