import * as ko from "knockout";
import * as $ from "jquery";

export class ContentDefinition{

    Row = ko.observableArray<Row>();
    BodyStyle = ko.observableArray<BodyStyle>([
        new BodyStyle('margin', '5%')
    ])
    visible = ko.observable<boolean>(true);
};

export class Row {
    Columns = ko.observableArray<Column>([new Column]);
    Css = new DefaultStyle();
    Attributes = [
        new AttributesKeyValue('width', '100%')
    ]
    IsRownDeleted = ko.observable<boolean>(true);
};

export class DefaultStyle{
    fontFamily: '';
    color: '#ffff';
}

export class AttributesKeyValue{
    key: string;
    value: string;

    constructor(key?: string, value?: string){
        this.key = key;
        this.value = value;
    }
};

export class Column{
    Content = ko.observable<string>("Content");
    width = ko.observable<string>("");
    height = ko.observable<string>("");
    Css = new DefaultStyle();
    Attributes = [
        new AttributesKeyValue('align', 'center')
    ]
    IsColumnDeleted = ko.observable<boolean>(true);
};

export class BodyStyle{
    key: string;
    value: string;

    constructor(key? : string , value?: string){
        this.key = key;
        this.value = value;
    }
};