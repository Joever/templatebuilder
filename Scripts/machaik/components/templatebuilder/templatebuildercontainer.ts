﻿import * as ko from 'knockout';
import {ContentDefinition, Row} from "./defaultmodel";

export class TemplateBuilderViewModel
{
    contentDefinition = new ContentDefinition();

    sections = this.contentDefinition.Row;

    constructor()
    {
        ko.computed(() =>
        {
            var secsRemoved = this.sections()
                .filter(c => c.IsRownDeleted() === false)[0];


            if (secsRemoved) {
                this.sections.remove(secsRemoved);
            }
            
            var secColCount = this.sections().filter(c =>c.Columns().length === 0)[0];
            if(secColCount){
                this.sections.remove(secColCount);
            }
        });
    }
    newSection = () =>
    {
        this.sections.push(new Row());        
    }
    SaveTemplate = () =>
    {
        console.log(ko.toJS(this.contentDefinition));
    }
}