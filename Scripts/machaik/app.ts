﻿import * as ko from "knockout";
import * as kom from 'knockout-mapping';
import "./components";
import "./bindinghandlers"; 
import * as $ from "jquery";   
import 'knockout.validation';
 


 
function init() {
 
    var viewModel = function () {
  
    };
    
 
    ko.applyBindings(new viewModel());
}

$(document).ready(init);
