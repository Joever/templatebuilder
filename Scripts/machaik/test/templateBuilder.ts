import * as ko from 'knockout';
import { expect } from "chai";
import { TemplateBuilderViewModel } from './../components/templatebuilder/templatebuildercontainer';
import { Row , Column } from './../components/templatebuilder/defaultmodel';
import { SectionViewModel }  from './../components/templatebuilder/sectionmodel';
import { ColumnsModel } from './../components/templatebuilder/columnmodel';
 
describe('TemplateBuilder', () => {

    
    it('should add new row/section when newSection method is trigger.', () => {
      let templateBuilderModel = new TemplateBuilderViewModel();
      templateBuilderModel.newSection();
      expect(templateBuilderModel.sections().length).to.eq(1);
    });

    
    it('should add new colum in row/section when addCol method is trigger.', () => {
      var visible = ko.observable(true);
      var section = new SectionViewModel(new Row()); //create default one column when initialized new row.
      var column = new ColumnsModel({visible : visible});
      section.addCol(column);
      section.addCol(column);
      section.addCol(column);
      expect(section.columns().length).to.equal(4);
    }); 

    it('should return width: 19% if 100% and column length-1 is 5 (100/5).', () => {      
      let percent = 100;
      let colLength = 5;
      let length = percent/colLength -1;
    
      expect('width:' + length + '%').to.equal('width:' + 19 + '%');      
    });

    it('should delete row/section when deleteSec is trigger.',() => {
      var section = new SectionViewModel(new Row()); //create default one column when initialized new row.
      section.deleteSec();
      expect(section.IsRownDeleted()).to.equal(false);
    });

    it('should delte column when deleteCol method is trigger', () => {
      var visible = ko.observable(true);
      var column = new ColumnsModel({visible : visible});
      column.deleteCol();
      expect(column.IsColumnDeleted()).to.equal(false);
    });
});
