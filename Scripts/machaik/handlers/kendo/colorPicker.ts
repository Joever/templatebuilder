import * as ko from "knockout";
import * as $ from "jquery";

export class KendoColorPicker {
    init = (el, va, ab, vm, bc) => {
        var vac = ko.unwrap(va());
        $(el).kendoColorPicker();
        var colorPicker = $(el).data("kendoColorPicker");
        colorPicker.value(vac); 
        colorPicker.bind("select", selected_color);
        function selected_color (e) {
            va()(e.value);
        }
    };
}

ko.bindingHandlers.kendoColorPicker = new KendoColorPicker();