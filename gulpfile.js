/// <binding Clean='browserify' ProjectOpened='browserify' />
var gulp = require('gulp'),
    browserify = require('browserify'),
    stringify = require('stringify'),
    source = require('vinyl-source-stream'),
    uglifycss = require('gulp-uglifycss'),
    concat = require('gulp-concat'),
    ts = require('gulp-typescript'),
    stripBom = require('gulp-stripbom'); 
    
gulp.task('browserify', ['typescript', 'copy'], function() {
    var bundleMethod = browserify; 

    var bundler = bundleMethod({ 
        entries: ['./scripts/build/app.js'],
        debug: false
    });

    var bundle = function() {
        return bundler 
            .transform(stringify(['.html']))
            .bundle()
            .on('error', function(err) {
                console.log(err.toString());
                this.emit("end");
            })
            .pipe(source('build.js'))
            .pipe(gulp.dest('./scripts/build/'));
    };

    return bundle();
});
 
gulp.task('watch', function() { 
    gulp.watch('./scripts/machaik/**/*.html', ['default']);
    gulp.watch('./scripts/machaik/**/*.ts', ['default']);
});
 
gulp.task('typescript', function() {
    gulp.src('./scripts/machaik/**/*.ts')
        .pipe(ts({
            noImplicitAny: false,
            allowJs: true,
            target: "es5"
        })).pipe(gulp.dest('./scripts/build'));;
});

gulp.task("copy",
    function() {
        return gulp.src([
                './scripts/machaik/*.html',
                './scripts/machaik/**/*.html',
                './scripts/machaik/**/*.js',
                './scripts/machaik/**/*.js'
            ])
            .pipe(stripBom())
            .pipe(gulp.dest('./Scripts/build/')); 
});

gulp.task('css', function() {
    gulp.src(['./css/cssfiles/*.css'])
        .pipe(concat('build.css'))
        .pipe(uglifycss())
        .pipe(gulp.dest('./css/build/'));
});

gulp.task('watch:css', function() {
    gulp.watch('./css/cssfiles/*.css', ['css']);
});

gulp.task('default', ['browserify', 'css', 'typescript']);